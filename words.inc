%include "list_config.inc"

; Макрос на добавление элемента в список
%macro add_key 1
%if  keySize = 8
    db %1, 0
%elif  keySize = 16
    dw %1, 0
%else
    dd %1, 0
%endif
%endmacro
; Глобальные метки на начало/конец списка
global list_start
global list_end
; Начало списка 
; Здесь простой список названия двух первых арабских цифр
; По два ключа цифра-слово, слово-цифра
list_start:

colon "One", key_1
add_key "1"

colon "Two", key_2
add_key "2"

colon "1", key_3
add_key "One"

colon "2", key_4
add_key "Two"
; Конец списка
list_end: