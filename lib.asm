; системные переменные
%define sys_exit 60
%define sys_read 0
%define sys_write 1
; ввод вывод
%define stdin 0
%define stdout 1
; код возврата
%define success 0
; константы
%define new_line '\n'
%define base_10 10

section .text
; глобальные метки на функции
global string_length
global print_char
global print_newline
global print_string
global print_error
global print_uint
global print_int
global string_equals
global parse_uint
global parse_int
global read_word
global string_copy
global exit

; Принимает код возврата и завершает текущий процесс
exit:
    xor rax, rax,
    mov rax, sys_exit
    syscall
    ret

sys_stdout:
    mov rax, sys_write
    mov rdi, stdout
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    ; rax = 0
    ; rdi -> value1 , ... value2, value3 ...
    .str_len_loop:
        ; проверка на конец строки
        cmp byte[rdi + rax], 0
        ; выходим если конец строки
        je .end
        ; иначе увеличиваем счетчик продолжаем цикл
        inc rax
        jmp .str_len_loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    ; сохраняем аргумент
    mov rsi, rdi
    ; подсчет длины строки
    call string_length
    mov rdx, rax
    call sys_stdout
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    ; указатель на стек
    mov rsi, rsp    
    ; печать одного символа
    mov rdx, 1      
    call sys_stdout
    add rsp, 8
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    push rsi
    mov rsi, new_line
    mov rdx, 1
    call sys_stdout
    pop rsi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r12
    mov r12, rsp
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0
    .loop:
        xor rdx, rdx
        dec rsp
        push r13
        mov r13, 10
        ; деление на 10(sys base) коэффициент в rdx
        div  r13                
        pop r13
        ; конверсия в ASCII
        add rdx, 0x30           
        mov  byte[rsp], dl      
        test rax, rax          
        ; установка флага(zf)
        jz .print
        jmp .loop
    .print:
        mov rdi, rsp
        call print_string
        mov rsp, r12
        jmp .end
    .end:
        pop r12
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    mov rax, rdi
    test rax, rax      
    jns .not_minus

    push rax
    ; печать знака минуса
    mov  rdi, '-' 
    call print_char
    pop rax
    ; перевод в положительное число
    neg rax 
    mov rdi, rax

    .not_minus:
        ; печатаем число, минус уже выведен
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
        xor rax, rax
        push r10
        push r13
        .loop:
            mov r10b, byte[rsi]
            mov r13b, byte[rdi]
            inc rsi
            inc rdi
            cmp r10b, r13b
            jne .ret_zero
            cmp r13b, 0
            jne .loop
            inc rax
       .ret_zero:
            pop r13 
            pop r10
                                                                                                                        ret
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdi
    push rdx
    dec rsp
    mov rax, sys_read
    mov rdi, stdin
    mov rdx, 1
    mov rsi, rsp
    syscall
    test rax, rax
    je .return
    xor rax, rax
    ; сохраняем значение в stack[rsp] 
    mov al, [rsp]

    .return:
        ; восстановление значений
        inc rsp
        pop rdx
        pop rdi
        ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
   push r14
   xor r14, r14
.M1:
   push rdi
   call read_char
   pop rdi
   cmp al, ' '
   je .M1
   cmp al, 10
   je .M1
   cmp al, 13
   je .M1
   cmp al, 9
   je .M1
.M2:
   mov byte [rdi + r14], al
   inc r14
   push rdi
   call read_char
   pop rdi
   cmp al, ' '
   je .M3
   cmp al, 10
   je .M3
   cmp al, 13
   je .M3
   cmp al, 9
   je .M3
   test al, al
   jz .M3
   cmp r14, 254
   je .M3
   jmp .M2
.M3:
   mov byte [rdi + r14], 0
   mov rax, rdi

   pop r14
   ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    push r13
    mov r13, 10
    xor rax, rax
    xor rcx, rcx
    xor rdx, rdx
    xor rsi, rsi
    .parse_char:
        mov sil, [rdi + rcx]
        cmp sil, 0x30
        jl .return
        cmp sil, 0x39
        jg .return
        inc rcx
        sub sil, 0x30
        mul r13
        add rax, rsi
        jmp .parse_char
    .return:
        mov rdx, rcx
        pop r13
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte [rdi], 0x2d
    je .parse_ng
    call parse_uint
    ret
    .parse_ng:
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .return
        neg rax
        inc rdx
    .return:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    ; by convention rdi, rsx, rdx, ...
    ; rdi - указатель на строку
    ; rsi - указатель на буфер
    ; rdx - длина бефера
    ; r12 регистр будет внутренним буфером
    ; сохраняем его в стеке
    push r12
    xor rcx, rcx
    .main_loop:
        ; for range 0 to buffer_len (проход по буферу)
        ; счетчик цикла
        cmp rcx, rdx    ; counter for loop
        ; если равно,то буфер переполнен
        je .overflow   
        ; копируем во внутренний буфер(start + offset)
        mov r12, [rdi + rcx]    
        ; копируем в выходную стркоу
        mov [rsi, rcx], r12     

        ; check for end of string
        cmp r12, 0
        je .exit

        inc rcx
        jmp .main_loop

    .overflow:
        mov rax, 0
        jmp .exit

    .exit:
        pop r12
        ret
